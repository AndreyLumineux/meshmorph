﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;
using UnityEngine.Events;
using UnityEngine.Serialization;
using ShowResult = UnityEngine.Advertisements.ShowResult;

public class AdButton : MonoBehaviour, IUnityAdsListener
{
    public float AdInterval => adInterval;
    public bool PlayOnce => playOnce;
    public GameObject internetWarningPopup;
    public bool CanPlayAd => (PlayOnce && !playedOnce || !PlayOnce) &&
        Time.realtimeSinceStartup > lastAdTime + adInterval;
    public bool HideIfNoAd => hideIfNoAd;
    public UnityEvent OnBeforeAdStart => onBeforeAdStart;
    public UnityEvent OnAdFailed => onAdFailed;
    public UnityEvent OnAdSkipped => onAdSkipped;
    public UnityEvent OnAdSuccessful => onAdSuccessful;
    public UnityEvent OnAdCompleted => onAdCompleted;

    [SerializeField] private float adInterval;
    [SerializeField] private bool playOnce;
    [SerializeField] private bool hideIfNoAd;
    [SerializeField] private UnityEvent onBeforeAdStart;
    [SerializeField] private UnityEvent onAdFailed;
    [SerializeField] private UnityEvent onAdSkipped;
    [SerializeField] private UnityEvent onAdSuccessful;
    [SerializeField] private UnityEvent onAdCompleted;
    private float lastAdTime;
    private bool playedOnce;
    private LevelController levelController;

	[FormerlySerializedAs("GameId")] public string gameId = "3075161";
	[FormerlySerializedAs("MyPlacementId")] public string myPlacementId = "rewardedVideo";
	[FormerlySerializedAs("TestMode")] public bool testMode = true;

	private void Awake()
    {
        lastAdTime = -adInterval;
        levelController = GameObject.FindWithTag("GameController").GetComponent<LevelController>();
    }

    private void Start()
    {
		Advertisement.AddListener(this);
		Advertisement.Initialize(gameId, testMode);
	    Monetization.Initialize(gameId, testMode);

		OnAdCompleted.AddListener(() =>
        {
            playedOnce = true;
            lastAdTime = Time.realtimeSinceStartup;
        });
        levelController.OnLevelRestarted.AddListener(() =>
        {
            lastAdTime = -adInterval;
            playedOnce = false;
            gameObject.SetActive(true);
        });
    }

    private void Update()
    {
        if (HideIfNoAd && !CanPlayAd && gameObject.activeSelf)
            gameObject.SetActive(false);
    }

    public void PlayAd()
    {
	    if (!CanPlayAd || Advertisement.isShowing || !Advertisement.IsReady(myPlacementId))
		    return;

	    if (Application.internetReachability == NetworkReachability.NotReachable)
		    internetWarningPopup.SetActive(true);
	    
	    OnBeforeAdStart.Invoke();
        Advertisement.Show(myPlacementId);
    }

	public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
	{
		if (placementId != myPlacementId)
			return;
		
		switch (showResult)
		{
			case ShowResult.Finished:
				OnAdSuccessful.Invoke();
				break;
			case ShowResult.Skipped:
				OnAdSkipped.Invoke();
				break;
			case ShowResult.Failed:
				OnAdFailed.Invoke();
				break;
			default:
				throw new ArgumentOutOfRangeException(nameof(showResult), showResult, null);
		}
		OnAdCompleted.Invoke();
	}

	public void OnUnityAdsReady(string placementId)
	{
		
	}

	public void OnUnityAdsDidError(string message)
	{
		// Log the error.
	}

	public void OnUnityAdsDidStart(string placementId)
	{
		// Optional actions to take when the end-users triggers an ad.
	}
}
