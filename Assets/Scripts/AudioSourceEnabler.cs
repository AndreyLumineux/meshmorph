﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceEnabler : MonoBehaviour
{
    public string PreferencesKey;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.mute = PlayerPrefs.GetInt(PreferencesKey, 1) == 0;
    }
}
