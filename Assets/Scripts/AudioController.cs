﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AudioController : MonoBehaviour
{
    public string PreferencesKey;
	public AudioSource AudioSource;
    public Image IconImage;
	public Sprite EnabledSprite;
    public Sprite DisabledSprite;
    public bool AudioEnabled
    {
        get => audioEnabled;
        set
        {
            audioEnabled = value;
            IconImage.sprite = value ? EnabledSprite : DisabledSprite;
            PlayerPrefs.SetInt(PreferencesKey, value ? 1 : 0);
			AudioSource.mute = !value;
        }
    }

    private Button button;
    private bool audioEnabled;

    private void Awake()
    {
        button = GetComponent<Button>();
        AudioEnabled = PlayerPrefs.GetInt(PreferencesKey, 1) != 0;
    }

    private void Start()
    {
        button.onClick.AddListener(() => { AudioEnabled = !AudioEnabled; });
    }
}
