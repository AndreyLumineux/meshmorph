﻿using UnityEngine;

public class Morpher : MonoBehaviour
{
	public GameObject MorphEffect;

	private Spawner spawner;

	private void Start()
	{
		spawner = GetComponent<Spawner>();
	}

	public void Morph(GameObject obj, GameObject target = null)
	{
		Vector3 pos = obj.transform.position;

		GameObject ef = Instantiate(MorphEffect, pos, Quaternion.identity);
		Destroy(ef, 2f);

		if(target)
		{
			spawner.Spawn(obj, pos, target);
		}
		else
		{
			spawner.RandomSpawn(obj, pos);
		}
	}
}
