﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

// TODO: Fix this script
public class TextGroupSize : MonoBehaviour
{
    public Text[] Texts;

    private void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        int minSize = Texts.Min(text => text.fontSize);
        foreach (Text text in Texts)
        {
            //text.resizeTextForBestFit = false;
            text.fontSize = minSize;
            text.resizeTextMaxSize = minSize;
        }
    }
}
