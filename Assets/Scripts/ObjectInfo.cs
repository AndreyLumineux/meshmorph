﻿using UnityEngine;

public class ObjectInfo : MonoBehaviour
{
	public GameObject FracturedObject;
	public GameObject WallPrefab;
	[SerializeField] private bool isWall;

    private void Start()
    {
		if (!isWall)
			Movement.Instance.ObjectInfo = this;
    }
}
