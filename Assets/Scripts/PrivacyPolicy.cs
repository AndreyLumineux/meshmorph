﻿using UnityEngine;

public class PrivacyPolicy : MonoBehaviour
{
	public string PolicyPreferenceKey = "PolicyAccepted";
	public GameObject PolicyMenu;

	private int policyAccepted;

	private void Start()
	{
		policyAccepted = PlayerPrefs.GetInt(PolicyPreferenceKey, 0);
		PolicyMenu.SetActive(policyAccepted == 0);
	}

	public void AcceptPolicy()
	{
		PlayerPrefs.SetInt(PolicyPreferenceKey, 1);
		PolicyMenu.SetActive(false);
	}
}
