﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
// ReSharper disable PossibleLossOfFraction

public class Movement : MonoBehaviour
{
    public float HorizontalSwapDuration = 0.3f;
    public bool AcceptInput = true;
    public UnityEvent OnDied;
    public UnityEvent OnFinishedLevel;
    [HideInInspector]
    public ObjectInfo ObjectInfo;

    private int nextDirection;
    private Coroutine horizontalMoveCoroutine;
	private Animator animator;
	private Rigidbody rb;

    private const float STEP = 2f;
    private const float MIN_X = -2f;
    private const float MAX_X = 2f;
    private const float HORIZONTAL_INPUT_QUEUE_THRESHOLD = 2/3f;
    private const float EPSILON = 1e-3f;
	private const float SPEED_MULTIPLICATION_PIVOT = 3f;

    public static Movement Instance { get; private set; }


	private float verticalSpeed = 10f;
	public float VerticalSpeed
	{
		get => verticalSpeed;
		set
		{
			verticalSpeed = value;

			if (!animator)
				animator = GetComponent<Animator>();

			animator.speed = value / 10f;

			HorizontalSwapDuration = SPEED_MULTIPLICATION_PIVOT / value;
		}
	}

	private void Awake()
	{
        if (Instance && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;
		animator = GetComponent<Animator>();
		rb = GetComponent<Rigidbody>();
	}
	
	private void Update()
    {
		//transform.Translate(Vector3.forward * Time.deltaTime * VerticalSpeed, Space.World);
		rb.MovePosition(rb.position + Time.deltaTime * VerticalSpeed * Vector3.forward);
        ProcessHorizontalInput();
    }

    private int GetHorizontalMoveDirection()
    {
        if (!AcceptInput)
            return 0;

#if UNITY_EDITOR
	    if (Input.GetKeyDown(KeyCode.LeftArrow))
		    return -1;
	    // ReSharper disable once ConvertIfStatementToReturnStatement
	    if (Input.GetKeyDown(KeyCode.RightArrow))
		    return 1;
#else
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(Input.touchCount - 1);
			if (touch.phase == TouchPhase.Began)
			{
				if (touch.position.x < Screen.width / 2)
					return -1;
				if (touch.position.x > Screen.width / 2)
					return 1;
			}
		}
#endif
	    return 0;
    }
	private void ProcessHorizontalInput()
    {
        if (!AcceptInput || nextDirection != 0 || horizontalMoveCoroutine != null)
            return;
        nextDirection = GetHorizontalMoveDirection();
        horizontalMoveCoroutine = StartCoroutine(HorizontalMove());
    }

    private IEnumerator HorizontalMove()
    {
        while (nextDirection != 0)
        {
            float initialX = Mathf.RoundToInt(rb.position.x);
            float targetX = initialX + STEP * nextDirection;
            nextDirection = 0;
            if (!(targetX >= MIN_X - EPSILON && targetX <= MAX_X + EPSILON))
                break;

            float distance = targetX - initialX;
            float elapsed = 0f;
            while (Mathf.Abs(rb.position.x - targetX) >= EPSILON)
            {
                float delta = Time.deltaTime / HorizontalSwapDuration * distance;
                elapsed += Time.deltaTime;
                float newX = Mathf.Clamp(rb.position.x + delta, MIN_X, MAX_X);
                if (Mathf.Sign(distance) * (targetX - newX) <= EPSILON || elapsed >= HorizontalSwapDuration)
					newX = targetX;

				//transform.SetPositionAndRotation(new Vector3(newX, transform.position.y, transform.position.z),
				//                transform.rotation);
				Vector3 currentPosition = rb.position;
				rb.MovePosition(new Vector3(newX, currentPosition.y, currentPosition.z));

                if (elapsed >= HorizontalSwapDuration * HORIZONTAL_INPUT_QUEUE_THRESHOLD && nextDirection == 0)
					nextDirection = GetHorizontalMoveDirection();
				yield return null;
            }
        }

        horizontalMoveCoroutine = null;
    }

	public void ResetMovement()
	{
		nextDirection = 0;

		if (horizontalMoveCoroutine != null)
			StopCoroutine(horizontalMoveCoroutine);
		horizontalMoveCoroutine = null;
	}
}
