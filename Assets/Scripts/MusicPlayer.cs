﻿using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
	public List<AudioClip> Clips;

	private AudioSource audioSource;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	public void PlayMusic()
	{
		int r = Random.Range(0, Clips.Count);
		audioSource.clip = Clips[r];
		audioSource.Play();
	}

	public void StopMusic()
	{
		audioSource.Stop();
	}
}
