﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;


public class Banner : MonoBehaviour
{
	public string GameId = "3075161";
	public string PlacementId = "bannerPlacement";
	public bool TestMode = true;

	private void Start()
	{
		Advertisement.Initialize(GameId, TestMode);
		Monetization.Initialize(GameId, TestMode);

		Advertisement.Banner.Load(PlacementId);
		StartCoroutine(ShowBannerWhenReady());
	}

	private IEnumerator ShowBannerWhenReady()
	{
		while (!Advertisement.IsReady(PlacementId))
			yield return new WaitForSeconds(0.5f);
		
		Advertisement.Banner.Show(PlacementId);
		Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
	}
}
