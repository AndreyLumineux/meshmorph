﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Interaction : MonoBehaviour
{
    public UnityEvent OnWallPass;
    public UnityEvent OnMorph;

    private const float MAX_VERTICAL_SPEED = 25f;
    private const float VERTICAL_SPEED_INCREMENT = 1 / 5f;
	private const int DEATHS_UNTIL_VIDEO = 5;
	private int remainingDeathsUntilVideo = DEATHS_UNTIL_VIDEO;

    private GameObject gameController;
    private LevelController levelController;
    private Destroyer destroyer;
    private Morpher morpher;

    private Movement movement;

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
        levelController = gameController.GetComponent<LevelController>();
        destroyer = gameController.GetComponent<Destroyer>();
        morpher = gameController.GetComponent<Morpher>();

        movement = Movement.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject childGameObject = transform.GetChild(transform.childCount - 1).gameObject;
        if (other.CompareTag("Pickup"))
        {
            Pickup pickup = other.GetComponent<Pickup>();
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (pickup.Type)
            {
                // Morpher pickup
                case 0:
                    OnMorph.Invoke();
                    morpher.Morph(childGameObject, pickup.TargetMesh);
                    other.gameObject.SetActive(false);
                    break;
                // Ending pickup
                case 1:
                    {
                        pickup.enabled = false;
                        if (movement != null)
                            movement.OnFinishedLevel?.Invoke();
                        break;
                    }
            }
        }
        else if (other.CompareTag("Wall"))
        {
            Transform parent = other.transform.parent;
            IEnumerable<GameObject> objects = from Transform child in parent
		        where child.gameObject.CompareTag("Wall")
		        select child.gameObject;
	        ObjectInfo[] infos = parent.GetComponentsInChildren<ObjectInfo>();
	        destroyer.FractureAll(objects, infos);

	        levelController.LastWallPassedPosition = other.transform.position;


			if (!CheckMeshWall(childGameObject.name, other.gameObject.name))
            {
                destroyer.Fracture(childGameObject, Movement.Instance.ObjectInfo.FracturedObject);
                PlayerDie();
            }
            else
            {
                OnWallPass.Invoke();

                if (movement.VerticalSpeed < MAX_VERTICAL_SPEED)
                {
                    movement.VerticalSpeed += VERTICAL_SPEED_INCREMENT;
                }
            }
        }
    }

    private bool CheckMeshWall(string mesh, string wall)
    {
        for (int i = 0; i < mesh.Length - 7; ++i) // "(Clone)" - 7 characters
            if (mesh[i] != wall[i])
                return false;
        return true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Deadly"))
            return;
        
        destroyer.Fracture(transform.GetChild(transform.childCount - 1).gameObject,
            GetComponentInChildren<ObjectInfo>().FracturedObject);
        PlayerDie();
    }

    public void PlayerDie()
    {
        GameObject player = gameObject;
        movement.OnDied?.Invoke();
        player.SetActive(false);

	    --remainingDeathsUntilVideo;
        if (remainingDeathsUntilVideo > 0)
            return;
        InterstitialVideo.Instance.ShowInterstitialVideo();
        remainingDeathsUntilVideo = DEATHS_UNTIL_VIDEO;
    }
}
