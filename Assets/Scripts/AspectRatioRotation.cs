using UnityEngine;
 
public class AspectRatioRotation : MonoBehaviour
{
    private void Start()
    {
        float width = Screen.width;
        float height = Screen.height;
        float hypotenuse = Mathf.Sqrt(width * width + height * height);
        float rotation = Mathf.Acos(width / hypotenuse) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + Vector3.forward * rotation);
    }
}
