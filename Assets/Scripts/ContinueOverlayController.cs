﻿using JetBrains.Annotations;
using UnityEngine;

public class ContinueOverlayController : MonoBehaviour
{
    private Movement movement;

    private void Awake()
    {
        movement = Movement.Instance;
    }

    [UsedImplicitly]
    public void EnablePlayer()
    {
        movement.enabled = true;

		gameObject.SetActive(false);
    }
}
