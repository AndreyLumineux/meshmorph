﻿using System.Collections;
using Cinemachine;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelController : MonoBehaviour
{
	private const float VERTICAL_SPEED_INCREMENT = 1 / 5f;
	public GameObject LevelUI;

	public string LevelPreferenceKey;
	public static int Seed;

	public CinemachineVirtualCamera CinemachineVirtualCamera;
	public GameObject DeathPopup;
	public GameObject FinishPopup;
	public GameObject EndEffect;
	public GameObject SettingsButton;
	public GameObject NoAdsButton;
	public GameObject PlayButton;
	public GameObject PrivacyPolicyButton;
	public GameObject EffectChange;
	public GameObject SettingsMenu;
	public GameObject NoAdsMenu;

	[HideInInspector]
	public Vector3 LastWallPassedPosition;

	private GameObject player;
	private Spawner spawner;
	private Movement movement;
	private Animator animator;
	private TutorialHandler tutorialHandler;
	private LevelGenerator levelGenerator;
	private Morpher morpher;

	private Coroutine deathPopupCoroutine;
	private Coroutine finishPopupCoroutine;

	private Text levelText;

	public UnityEvent OnLevelRestarted => onLevelRestarted;
    [SerializeField] private UnityEvent onLevelRestarted;
	public UnityEvent OnLevelStarted => onLevelStarted;
	[SerializeField] private UnityEvent onLevelStarted;
	private static readonly int FadeIn = Animator.StringToHash("FadeIn");

    private void Awake()
	{
		tutorialHandler = GetComponent<TutorialHandler>();
		levelGenerator = GetComponent<LevelGenerator>();
		spawner = GetComponent<Spawner>();
		morpher = GetComponent<Morpher>();

		if (LevelUI)
			levelText = LevelUI.GetComponent<Text>();
	
		player = GameObject.FindGameObjectWithTag("Player");
        movement = Movement.Instance;
        if (player)
	        animator = player.GetComponent<Animator>();

        if (Seed == 0)
			Reseed();
        
		Init();
	}
    
#if DEBUG
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha2))
			AddLevel(1);
		else if (Input.GetKeyDown(KeyCode.Alpha1))
			AddLevel(-1);
	}
#endif

	private static void Reseed()
	{
		Seed = Random.Range(int.MinValue, int.MaxValue);
	}

	private void Init()
	{
		Random.InitState(Seed);
		if (levelText)
			levelText.text = PlayerPrefs.GetInt(LevelPreferenceKey, 1).ToString();

		if (movement)
			movement.VerticalSpeed = VERTICAL_SPEED_INCREMENT * PlayerPrefs.GetInt(LevelPreferenceKey, 1) + 10;
	}

	public void StartLevel()
	{
		PlayButton.SetActive(false);
		SettingsButton.SetActive(false);
		NoAdsButton.SetActive(false);
		PrivacyPolicyButton.SetActive(false);
		EffectChange.SetActive(false);

		LastWallPassedPosition = spawner.SpawnPos;

		if (tutorialHandler.TutorialStatus == TutorialStatus.Inactive)
			tutorialHandler.StartTutorial();
		else
			UnfreezePlayer();
		OnLevelStarted.Invoke();
	}

	public void RestartCurrentLevel()
	{
		CinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 0f;
		
		player.SetActive(true);
		player.transform.position = spawner.SpawnPos;
		spawner.RandomSpawn(null, spawner.SpawnPos);

		Init();
		levelGenerator.DestroyActiveLevel();
		levelGenerator.GenerateLevel();

		DeathPopup.SetActive(false);
		FinishPopup.SetActive(false);

		UnfreezePlayer();

		Invoke(nameof(CameraYDampingBack), 0.2f);
        OnLevelRestarted.Invoke();
	}

	private void CameraYDampingBack()
	{
		CinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 0.5f;
	}

	public void NewLevel()
	{
		Reseed();
		RestartCurrentLevel();
		OnLevelStarted.Invoke();
	}

	public void ContinueLevel()
	{
		player.SetActive(true);

		CinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 0f;

		Vector3 position = LastWallPassedPosition;
		position.y = player.transform.position.y;

		position.x = 0f;
		player.transform.SetPositionAndRotation(position, player.transform.rotation);

		GameObject effect = Instantiate(morpher.MorphEffect, position, Quaternion.identity);
		Destroy(effect, 2f);

		DeathPopup.SetActive(false);

		UnfreezePlayer();

		Invoke(nameof(CameraYDampingBack), 0.2f);
		OnLevelStarted.Invoke();
	}

	public void EnableDelayedDeathPopup(float time)
	{
		if (deathPopupCoroutine != null)
			StopCoroutine(deathPopupCoroutine);

		deathPopupCoroutine = StartCoroutine(nameof(DeathPopupCoroutine), time);
	}

	private IEnumerator DeathPopupCoroutine(float time)
	{

		yield return new WaitForSeconds(time);
		DeathPopup.SetActive(true);
		DeathPopup.GetComponent<Animator>()?.SetTrigger(FadeIn);
	}

	public void EnableDelayedFinishPopup(float time)
	{
		if (finishPopupCoroutine != null)
			StopCoroutine(finishPopupCoroutine);

		finishPopupCoroutine = StartCoroutine(nameof(FinishPopupCoroutine), time);
	}

	private IEnumerator FinishPopupCoroutine(float time)
	{
		yield return new WaitForSeconds(time);
		FinishPopup.SetActive(true);
		FinishPopup.GetComponent<Animator>()?.SetTrigger(FadeIn);
	}

	public void EndLevel()
	{
		GameObject effect = Instantiate(EndEffect, player.transform.position, Quaternion.identity);
		Destroy(effect, 2f);
		Destroy(player.transform.GetChild(player.transform.childCount - 1).gameObject);

		player.SetActive(false);
		EnableDelayedFinishPopup(1 / 2f);

		PlayerPrefs.SetInt(LevelPreferenceKey, PlayerPrefs.GetInt(LevelPreferenceKey, 1) + 1);
		OnLevelRestarted.Invoke();
	}

	[UsedImplicitly]
	public void OpenSettings()
	{
		SettingsMenu.SetActive(true);
	}

	[UsedImplicitly]
	public void CloseSettings()
	{
		SettingsMenu.SetActive(false);
	}

	public void OpenNoAds()
	{
		NoAdsMenu.SetActive(true);
	}

	public void CloseNoAds()
	{
		NoAdsMenu.SetActive(false);
	}

	public void FreezePlayer()
	{
		movement.enabled = false;
		animator.enabled = false;
	}

	public void UnfreezePlayer()
	{
		movement.enabled = true;
		movement.ResetMovement();
		animator.enabled = true;
	}

	public void AddLevel(int inc)
	{
		spawner.RandomSpawn(player.transform.GetChild(1).gameObject, spawner.SpawnPos);

		PlayerPrefs.SetInt(LevelPreferenceKey, PlayerPrefs.GetInt(LevelPreferenceKey, 1) + inc);
		Reseed();
		Init();
		levelGenerator.DestroyActiveLevel();
		levelGenerator.GenerateLevel();
	}
}
