﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trail : MonoBehaviour
{
	public List<GameObject> Trails;
	public List<GameObject> ShowcaseTrails;
	public static Trail Instance { get; private set; }

	public Button LeftButtonTrailChanger;
	public Button RightButtonTrailChanger;

	private LevelController levelController;
	private int selectedTrail;

	private void Awake()
	{
		if (Instance && Instance != this)
		{
			Destroy(this);
			return;
		}
		Instance = this;

		levelController = GameObject.FindWithTag("GameController").GetComponent<LevelController>();
	}

	private void Start()
	{
		selectedTrail = PlayerPrefs.GetInt(levelController.LevelPreferenceKey, 1) / 5;
		IncrementPlayerShowcaseTrail(0);
	}

	public bool IsValidTrail(int trail)
	{
		if (trail < 0)
			return false;
		if (trail > Trails.Count - 1)
			return false;
		return trail <= PlayerPrefs.GetInt(levelController.LevelPreferenceKey, 1) / 5;
	}

	public void AddPlayerTrail()
	{
		foreach (Transform child in transform)
			Destroy(child.gameObject);

		Instantiate(Trails[selectedTrail], transform);
		
		//int playerLevel = PlayerPrefs.GetInt(levelController.LevelPreferenceKey, 1);
		//int max = Mathf.Min(Trails.Count - 1, playerLevel / 5);

		//Instantiate(Trails[max], transform);
	}

	public void IncrementPlayerShowcaseTrail(int inc)
	{
		if (!IsValidTrail(selectedTrail + inc))
			return;
		
		selectedTrail += inc;

		foreach (Transform child in transform)
			Destroy(child.gameObject);

		Instantiate(ShowcaseTrails[selectedTrail], transform);
	}
}
