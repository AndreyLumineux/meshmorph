﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
	public List<GameObject> Objects;
	public Vector3 SpawnPos;

	private GameObject player;
	private int playerLevel;

	private void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		playerLevel = PlayerPrefs.GetInt(GetComponent<LevelController>().LevelPreferenceKey, 1);

		RandomSpawn(null, SpawnPos);
	}

	public void RandomSpawn(GameObject existingObject, Vector3 pos)
	{
		int count = player.transform.childCount;
		if (count > 1)
		{
			Transform shapeTransform = player.transform.GetChild(count - 1);
			Destroy(shapeTransform.gameObject);
		}

		int max = 4 + playerLevel / 5 < Objects.Count ? 4 + playerLevel / 5 : Objects.Count;
		int r = Random.Range(0, max);
		if (existingObject)
		{
			while (existingObject.name.Contains(Objects[r].name))
			{
				r = Random.Range(0, max);
			}
		}
		player.transform.localScale = Vector3.one;
		GameObject obj = Instantiate(Objects[r], pos, Objects[r].transform.rotation, player.transform);
		obj.transform.localScale = Vector3.one;
		obj.transform.localPosition = Vector3.zero;
		obj.SetActive(true);

		if (existingObject)
		{
			Destroy(existingObject);
		}
	}

	public void Spawn(GameObject existingObject, Vector3 pos, GameObject targetObject)
	{
		GameObject obj = Instantiate(targetObject, pos, targetObject.transform.rotation, player.transform);
		obj.transform.localScale = Vector3.one;
		obj.SetActive(true);

		if (existingObject)
		{
			Destroy(existingObject);
		}
	}
}
