﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
	//public float explosionForceMin = 1f;
	//public float explosionForceMax = 5f;
	//public float explosionRadius = 1f;
	//public float WallForceMultiplier = 2f;

	public void FractureAll(IEnumerable<GameObject> objects, IEnumerable<ObjectInfo> infos)
	{
		IEnumerable<GameObject> gameObjects = objects as GameObject[] ?? objects.ToArray();
		IEnumerable<ObjectInfo> objectInfos = infos as ObjectInfo[] ?? infos.ToArray();
		int length = gameObjects.Count();
		for (int i = 0; i < length; ++i)
			Fracture(gameObjects.ElementAt(i), objectInfos.ElementAt(i).FracturedObject);
	}

	public void Fracture(GameObject shape, GameObject fracturedPrefab)
	{
		Vector3 pos = shape.transform.position;

		GameObject fracturedObject = Instantiate(fracturedPrefab, pos, fracturedPrefab.transform.rotation);

		bool isWall = shape.CompareTag("Wall");

		if(isWall)
			shape.SetActive(false);

		//float forceMin = isWall ? explosionForceMin * WallForceMultiplier : explosionForceMin;
		//float forceMax = isWall ? explosionForceMax * WallForceMultiplier : explosionForceMax;

		//foreach (Transform shard in fracturedObject.transform)
		//{
		//	shard.GetComponent<Rigidbody>().AddExplosionForce(Random.Range(forceMin, forceMax),
		//													  shard.position - Vector3.forward - Vector3.up,
		//													  explosionRadius);
		//}
		Destroy(fracturedObject, 3f);
	}
}
