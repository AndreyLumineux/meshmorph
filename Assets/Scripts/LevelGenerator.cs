﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour
{
	public GameObject SimpleChunk;
	public GameObject EndingChunk;
	public bool ActiveGenerating = true;
	public float OffsetZ = 40f;
	public List<GameObject> Walls;
	public List<GameObject> Pickups;
	public List<GameObject> Obstacles;

	private List<GameObject> activeChunks;
	private List<GameObject> activeObstacles;
	private float generatorZPos;
	private GameObject lastMorphPickupTargetMesh;
	private GameObject beforeLastMorphPickupTargetMesh;
	private Spawner spawner;
	private GameObject player;
	private LevelController levelController;
	private int playerLevel;

	private void Start()
	{
		levelController = GetComponent<LevelController>();
		spawner = GetComponent<Spawner>();
		player = GameObject.FindGameObjectWithTag("Player");

		GenerateLevel();
	}

	private void Update()
	{
		if (ActiveGenerating && player.transform.position.z > generatorZPos - OffsetZ)
			GenerateChunk();
	}

	public void GenerateLevel()
	{
		levelController.FreezePlayer();

		generatorZPos = 0f;

		activeChunks = new List<GameObject>();
		activeObstacles = new List<GameObject>();

		playerLevel = PlayerPrefs.GetInt(levelController.LevelPreferenceKey);


		GenerateChunk(ChunkTypes.Simple);
		int level = PlayerPrefs.GetInt(levelController.LevelPreferenceKey);
		if (ActiveGenerating)
			return;
		
		for (int i = 0; i < 5 + level; i++)
			GenerateChunk();
		GenerateEndingChunk();
	}

	public void DestroyActiveLevel()
	{
		foreach (GameObject o in activeChunks.Union(activeObstacles))
		{
			Destroy(o);
		}

		lastMorphPickupTargetMesh = null;
		beforeLastMorphPickupTargetMesh = null;
	}

	private void GenerateEndingChunk()
	{
		GameObject chunk = Instantiate(EndingChunk);
		generatorZPos += chunk.transform.lossyScale.z;
		chunk.transform.position = Vector3.forward * generatorZPos;

		activeChunks.Add(chunk);
	}

	private void GenerateChunk(ChunkTypes? chunkType = null)
	{
		ChunkTypes r;
		if (chunkType == null)
		{
			// Starting from 1 so that empty chunks are not generated
			int length = Enum.GetValues(typeof(ChunkTypes)).Length;
			int max = Mathf.Min(3 + playerLevel / 5, length);
			r = (ChunkTypes)Random.Range(1, max);
		}
		else
			r = chunkType.Value;

		GameObject chunk = Instantiate(SimpleChunk);

		float lossyScaleZ = chunk.transform.lossyScale.z;
		generatorZPos += activeChunks.Count == 0 
			? lossyScaleZ / 2 
			: lossyScaleZ;
		chunk.transform.position = Vector3.forward * generatorZPos;

		FillChunk(chunk, r);

		activeChunks.Add(chunk);
		GameObject firstCreated = activeChunks.Last();

		if (!(firstCreated.transform.position.z < player.transform.position.z - OffsetZ))
			return;
		
		activeChunks.Remove(firstCreated);
		Destroy(firstCreated);
	}

	private void FillChunk(GameObject chunk, ChunkTypes chunkType)
	{
		switch (chunkType)
		{
			case ChunkTypes.Simple: // Simple - 3 random walls
				break;
			case ChunkTypes.Morpher: // Morpher - 3 random walls + 1 morph pickup
				AddPickup(chunk);
				break;
			case ChunkTypes.Spiky: // Spiky - 3 random walls + 2 spikes
				AddObstacles(ObstacleTypes.Spike, chunk, 2, true);
				break;
			case ChunkTypes.Crusher: // Crusher - 3 random walls + 1 crusher
				AddObstacles(ObstacleTypes.Crusher, chunk);
				break;
			case ChunkTypes.DualCrusher: // DualCrusher - 3 random walls + 2 crushers
				AddObstacles(ObstacleTypes.Crusher, chunk, 2, true);
				break;
			case ChunkTypes.SpikyCrusher: // SpikyCrusher - 3 random walls + 1 spike + 1 crusher
				AddObstacles(ObstacleTypes.Spike, chunk, 1, true, 4);
				AddObstacles(ObstacleTypes.Crusher, chunk);
				break;
			default:
				throw new ArgumentOutOfRangeException(nameof(chunkType), chunkType, null);
		}
		AddWalls(chunk);
	}

	private void AddWalls(GameObject chunk)
	{
		List<GameObject> localList = new List<GameObject>();
		int length = Walls.Count;
		int max = Mathf.Min(4 + playerLevel / 5, length);
		localList.AddRange(Walls.GetRange(0, max));

		int childCount = player.transform.childCount;
		Transform lastChildTransform = player.transform.GetChild(childCount - 1);
		ObjectInfo childObjectInfo = lastChildTransform.GetComponent<ObjectInfo>();

		int rightWallX = Random.Range(-1, 2) * 2;

		GameObject rightWallPrefab = lastMorphPickupTargetMesh
			? lastMorphPickupTargetMesh.GetComponent<ObjectInfo>().WallPrefab
			: childObjectInfo.WallPrefab;

		GameObject wrongWallPrefab = beforeLastMorphPickupTargetMesh
			? beforeLastMorphPickupTargetMesh.GetComponent<ObjectInfo>().WallPrefab
			: lastMorphPickupTargetMesh
				? childObjectInfo.WallPrefab
				: null;
		localList.Remove(wrongWallPrefab);


		GameObject wall = Instantiate(rightWallPrefab, chunk.transform, true);
		localList.Remove(rightWallPrefab);
		float z = chunk.transform.position.z + chunk.transform.lossyScale.z / 2;
		wall.transform.position = new Vector3(rightWallX, wall.transform.position.y, z);
		
		for (int i = -1; i <= 1; ++i)
		{
			if (i * 2 == rightWallX) continue;
			int wallIndex = Random.Range(0, localList.Count);
			wall = Instantiate(localList[wallIndex], chunk.transform, true);
			localList.Remove(localList[wallIndex]);
			wall.transform.position = new Vector3(i * 2, wall.transform.position.y, z);
		}
	}

	private void AddPickup(GameObject chunk)
	{
		List<float> possibleXPositions = new List<float> { -2, 0, 2 };
		float xPos = possibleXPositions[Random.Range(0, possibleXPositions.Count)];

		int pickupIndex = Random.Range(0, Pickups.Count);
		GameObject pickup = Instantiate(Pickups[pickupIndex], chunk.transform, true);

		float z = chunk.transform.position.z - chunk.transform.lossyScale.z / 6;
		pickup.transform.position = new Vector3(xPos, pickup.transform.position.y, z);

		int max = Mathf.Min(4 + playerLevel / 5, spawner.Objects.Count);
		int randomIndex = Random.Range(0, max);
		while (lastMorphPickupTargetMesh && spawner.Objects[randomIndex] == lastMorphPickupTargetMesh)
			randomIndex = Random.Range(0, max);
		
		while (!lastMorphPickupTargetMesh &&
				MeshesFromSamePrefab(spawner.Objects[randomIndex].name,
									 player.transform.GetChild(player.transform.childCount - 1).name))
			randomIndex = Random.Range(0, max);

		pickup.GetComponent<Pickup>().TargetMesh = spawner.Objects[randomIndex];
		beforeLastMorphPickupTargetMesh = lastMorphPickupTargetMesh;
		lastMorphPickupTargetMesh = spawner.Objects[randomIndex];

		pickup.SetActive(true);
	}

	private void AddObstacles(ObstacleTypes obstacleType, GameObject chunk, int n = 1, bool randomZ = false, float chunkScaleRatio = 6f)
	{
		List<float> possibleXPositions = new List<float> { -2, 0, 2 };
		for (int i = 0; i < n; i++)
		{
			float xPos = 0f;
			if (obstacleType != ObstacleTypes.Crusher)
			{
				xPos = possibleXPositions[Random.Range(0, possibleXPositions.Count)];
				possibleXPositions.Remove(xPos);
			}
			
			GameObject obstacle = Instantiate(Obstacles[(int) obstacleType]);

			float z = chunk.transform.position.z;
			if (randomZ)
			{
				int random = Random.Range(0, 2);
				z = chunk.transform.position.z + 2 * (0.5f - random) * chunk.transform.lossyScale.z / chunkScaleRatio;
			}
			obstacle.transform.position = new Vector3(xPos, obstacle.transform.position.y, z);
			obstacle.SetActive(true);

			activeObstacles.Add(obstacle);
		}
	}

	private static bool MeshesFromSamePrefab(string meshInList, string meshInScene)
	{
		return !meshInList.Where((c, i) => c != meshInScene[i]).Any();
	}
}


internal enum ChunkTypes
{
	Simple,
	Morpher,
	Spiky,
	Crusher,
	DualCrusher,
	SpikyCrusher
}

internal enum ObstacleTypes
{
	Spike,
	Crusher
}