﻿using System;
using UnityEngine;

public class TutorialHandler : MonoBehaviour
{
	public string TutorialStatusKey;
	public GameObject LeftTutorial;
	public GameObject RightTutorial;
	public GameObject TutorialButton;
	public TutorialStatus TutorialStatus = TutorialStatus.Inactive;

	private float lastActionTime;

	private void Start()
	{
		if(PlayerPrefs.GetInt(TutorialStatusKey, 0) == 1)
		{
			TutorialStatus = TutorialStatus.Done;
		}
	}

	public void StartTutorial()
	{
		LeftTutorial.SetActive(true);
		TutorialStatus = TutorialStatus.Left;

		TutorialButton.SetActive(true);

		lastActionTime = Time.realtimeSinceStartup;
	}

	public void TutorialNextStep()
	{
		float elapsed = Time.realtimeSinceStartup;
		switch (TutorialStatus)
		{
			case TutorialStatus.Left when elapsed - lastActionTime > 1f:
				LeftTutorial.SetActive(false);
				RightTutorial.SetActive(true);
				TutorialStatus = TutorialStatus.Right;

				lastActionTime = elapsed;
				break;
			case TutorialStatus.Right when elapsed - lastActionTime > 1f:
				TutorialButton.SetActive(false);
				RightTutorial.SetActive(false);
				TutorialStatus = TutorialStatus.Done;

				GetComponent<LevelController>().UnfreezePlayer();

				PlayerPrefs.SetInt(TutorialStatusKey, 1);
				break;
			case TutorialStatus.Inactive:
			case TutorialStatus.Done:
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}
}

public enum TutorialStatus
{
	Inactive,
	Left,
	Right,
	Done
}