﻿using JetBrains.Annotations;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
	public AudioClip WallPassClip;
	public AudioClip DeathClip;
	public AudioClip LevelFinishClip;
	public AudioClip MorphingClip;

	private AudioSource audioSource;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	[UsedImplicitly]
	public void PlayClip(string clipName)
	{
		switch (clipName)
		{
			case "WallPass":
				audioSource.PlayOneShot(WallPassClip);
				break;
			case "Death":
				audioSource.PlayOneShot(DeathClip);
				break;
			case "LevelFinish":
				audioSource.PlayOneShot(LevelFinishClip);
				break;
			case "Morph":
				audioSource.PlayOneShot(MorphingClip);
				break;
			default:
				Debug.LogError("PlayClip clip name not found: " + clipName);
				break;
		}
	}
}
