﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;

public class InterstitialVideo : MonoBehaviour
{
	public static InterstitialVideo Instance { get; private set; }

	public string GameId = "3075161";
	public string PlacementId = "video";
	public bool TestMode = true;

	private void Awake()
	{
		if (Instance && Instance != this)
		{
			Destroy(this);
			return;
		}

		Instance = this;
	}

	private void Start()
	{
		Advertisement.Initialize(GameId, TestMode);
		Monetization.Initialize(GameId, TestMode);
	}

	public void ShowInterstitialVideo()
	{
		StartCoroutine(DelayedVideo());
	}

	private IEnumerator DelayedVideo()
	{
		yield return new WaitForSeconds(0.5f);
		Advertisement.Show(PlacementId);
	}
}
