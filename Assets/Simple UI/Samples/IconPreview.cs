﻿using UnityEngine;
using UnityEngine.UI;

namespace Simple_UI.Samples
{
	public class IconPreview : MonoBehaviour {
		public Sprite[] Icons;
		private GameObject icon;
		
		private void Awake () {
			for (int i = 0; i < Icons.Length; i++) {
				icon = new GameObject ("icon" + i);
				icon.transform.SetParent(gameObject.transform);
				icon.AddComponent<RectTransform> ();
				icon.AddComponent<Image> ();
				icon.GetComponent<Image> ().sprite = Icons [i];
			}
		}
	}
}
